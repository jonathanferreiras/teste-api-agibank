const express = require('express')
const routes = express.Router()

const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        description: "Esta é apenas uma api para o teste do banco agibank",
        version: "1.0.0",
        title: "Teste API - Agibank",
        termsOfService: "http://swagger.io/terms/",
        contact: {
            email: "jonathanferreiras@outlook.com"
        },
           
        license: {
            name: "Apache 2.0",
            url: "http://www.apache.org/licenses/LICENSE-2.0.html"
        }
            
      },
    },
    
    // ['.routes/*.js']
    apis: ['./src/app/routes.js']
  };
  
  const swaggerDocs = swaggerJsDoc(swaggerOptions);
  routes.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));



const InvestimentController = require('./controllers/InvestmentController')
const UserController = require('./controllers/UserController')


/**
 * @swagger
 * 
 * 
 * /user:
 *    description: Criar um usuário
 * 
 *    post:
 *      tags:
 *          - User     
 *      description: Criar um novo usuário   
 *    responses:
 *      '200':
 *        description: Usuário criado com sucesso
 */
routes.post('/user', UserController.create)

/**
 * @swagger
 * 
 * 
 * /user:
 *    description: Listar usuários
 * 
 *    get:
 *      tags:
 *          - User     
 *      description: Listar todos os usuários 
 *    responses:
 *      '200':
 *        description: Usuários listados com sucesso
 */
routes.get('/user', UserController.get)

/**
 * @swagger
 * 
 * 
 * /user/{name}:
 *    description: listar um usuário
 * 
 *    get:
 *      tags:
 *          - User     
 *      description: Listar um usuário
 *    responses:
 *      '200':
 *        description: Usuário listado com sucesso
 */
routes.get('/user/:name', UserController.getUser)


/**
 * @swagger
 * 
 * 
 * /user/{name}:
 *    description: Deletar um usuário
 * 
 *    delete:
 *      tags:
 *          - User     
 *      description: Deletar um usuário
 *    responses:
 *      '200':
 *        description: Usuário deletado com sucesso
 */
routes.delete('/user/:name', UserController.delete)


/**
 * @swagger
 * 
 * 
 * /investment:
 *    description: Criar um investimento
 * 
 *    post:
 *      tags:
 *          - Investment     
 *      description: Criar um novo investimento para o usuário   
 *    responses:
 *      '200':
 *        description: Investimento criado com sucesso
 */
routes.post('/investment', InvestimentController.create)

/**
 * @swagger
 * 
 * 
 * /investment:
 *    description: Listar investimentos
 * 
 *    get:
 *      tags:
 *          - Investment     
 *      description: Listar todos os investimentos.
 *    responses:
 *      '200':
 *        description: Investimentos listado com sucesso
 */
routes.get('/investment', InvestimentController.get)

/**
 * @swagger
 * 
 * 
 * /investment/user:
 *    description: Listar investimentos de um usuário
 * 
 *    get:
 *      tags:
 *          - Investment     
 *      description: Listar todos os investimentos de um único usuário.
 *    responses:
 *      '200':
 *        description: Investimentos listado com sucesso
 */
routes.get('/investment/:user', InvestimentController.getInvestment)

/**
 * @swagger
 * 
 * 
 * /investment/user/{user}:
 *    description: Deletar todos investimentos de um usuário
 * 
 *    delete:
 *      tags:
 *          - Investment     
 *      description: Deletar todos investimentos de um único usuário.
 *    responses:
 *      '200':
 *        description: Investimentos deletados com sucesso
 */
routes.delete('/investment/user/:user', InvestimentController.deleteUser)

/**
 * @swagger
 * 
 * 
 * /investment/{id}:
 *    description: Deletar um investimento.
 * 
 *    delete:
 *      tags:
 *          - Investment     
 *      description: Deletar um investimento.
 *    responses:
 *      '200':
 *        description: Investimento deletado com sucesso
 */
routes.delete('/investment/:id', InvestimentController.delete)




// routes.post('/account', AccountController.create)
// routes.get('/account', AccountController.get)
// routes.get('/account/:name', AccountController.getName)
// routes.delete('/account/:name', AccountController.delete)


module.exports = routes