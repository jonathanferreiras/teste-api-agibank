const assert = require('assert')
// const userController = require('./controllers/UserController')
const testController = require('./controllers/TestController')

describe('API TESTS', () => {
    it('Criar Usuário novo', async () => {
        const expected = {
            "name": "teste",
            "account": "12345678",
            "agency": "12",
            "balance": 123,
        }
        const base = {           
            "name": "teste",
            "account": "12345678",
            "agency": "12",
            "balance": 123,              
        }
        const result = await testController.RequestPost(base,'user')
        //console.log(result)
        global.userId = result._id
        const resultMap = testController.mapUser(result)
        assert.deepEqual(resultMap,expected)        
    }),

    it('Listar um usuário', async () => {
        const expected = {
            "name": "teste",
            "account": "12345678",
            "agency": "12",
            "balance": 123,
        }

        const base = "teste"
        const result = await testController.RequestGet(null,'user/' + base)
        const resultMap = testController.mapUser(result)
        assert.deepEqual(resultMap,expected)
    }),

    it('Listar um usuário que não existe', async () => {
        const expected = {
            "name": "teste não existe",
            "account": "12345678",
            "agency": "12",
            "balance": 123,
        }

        const base = "teste"
        const result = await testController.RequestGet(null,'user/' + base)
        const resultMap = testController.mapUser(result)
        assert.deepEqual(resultMap,expected)
    }),

    it('Criar um Investimento', async () => {
        const expected = {            
            "balance": 10000,
            "type": "cdb"
        }

        const base = {
            "name": "teste",
            "balance": 10000,
            "type": "cdb"
        }

        const result = await testController.RequestPost(base,'investment')
        
        const resultMap = testController.mapInvestment(result)
        assert.deepEqual(resultMap,expected)       
    }),

    it('Listar um investimento', async () => {
        const expected = {               
            "balance": 10000,
            "type": "cdb"
        }

        base = global.userId
        
        const result = await testController.RequestGet(null,'investment/' + base)       
        const resultMap = testController.mapInvestment(result.docs[0])       
        assert.deepEqual(resultMap,expected)       
    }),

    it('Listar um investimento que não existe', async () => {
        const expected = {               
            "balance": 10000,
            "type": "fiis"
        }

        base = global.userId
        
        const result = await testController.RequestGet(null,'investment/' + base)       
        const resultMap = testController.mapInvestment(result.docs[0])       
        assert.deepEqual(resultMap,expected)       
    }),

    it('Excluir investimentos de um usuário', async () => {
        const expected = 200

        base = global.userId
        
        const result = await testController.RequestDelete(null,'investment/user/' + base)       
               
        assert.deepEqual(result,expected)       
    }),

    it('Excluir um usuário', async () => {
        const expected = 200

        base = global.userId
        
        const result = await testController.RequestDelete(null,'user/' + base)       
               
        assert.deepEqual(result,expected)       
    })

})