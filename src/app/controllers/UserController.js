const mongoose = require('mongoose');

const User = require('../models/User')

module.exports = {

    /**
     * Realiza o cadastro do usuário.
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async create(request, response) {

        var user = await User.findOne(request.body);
        
        if (! user) {
            try {
                user = await User.create(request.body);
            } catch (exception) {
                return response.status(406).send(exception.message);                            
        }      
        
    }
        return response.json(user);
    },

    /**
     * Retorna dados do usuário
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async getUser(request, response) {
        
        try {
            var user = await User.findOne({
                name: request.params.name
            });
            
        } catch (exception) {
            return response.status(406).send(exception.message);
        }
        response.json(user);
    },

    /**
     * Retorna id do usuário
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async getUserId(request) {

        try {
            var user = await User.findOne({
                name: request.body.name
            });
        } catch (exception) {
            return exception.message
        }       

        return user
    },

     /**
     * Retorna dados de todos usuário
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async get(request, response) {

        const { page = 1 } = request.query;
        const { limit = 10 } = request.query;

        try {
            var user = await User.paginate({},{
                page, 
                limit                
            });
        } catch (exception) {
            return response.status(406).send(exception.message);
        }       

        response.json(user);
    },
    
    
    /**
     * Remove usuário
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async delete(request, response) {
        try {
            await User.findOneAndDelete({name: request.params.name});
        } catch (exception) {
            return response.status(406).send(exception.message);
        }
        response.send();
    }
};