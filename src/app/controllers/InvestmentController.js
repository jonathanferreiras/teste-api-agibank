const mongoose = require('mongoose');
const Investment = require('../models/Investment')
const UserController = require('./UserController')

module.exports = {

    /**
     * Realiza o cadastro do investimento.
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async create(request, response) {

        var user = await UserController.getUserId(request)   

        if (!user) {
            return response.status(400).json({ error: 'User does not exists' });
          }
       
        

        var investment = await Investment.findOne({
            user: user._id,
            balance: request.body.balance,
            type: request.body.type,
        });
        
        if (! investment) {
            try {
                investment = await Investment.create({
                    user: user._id,
                    balance: request.body.balance,
                    type: request.body.type,
                });
            } catch (exception) {
                return response.status(406).send(exception.message);                            
        }      
        
    }
         response.json(investment);
    },

    /**
     * Retorna dados do investimento do usuário específico
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async getInvestment(request, response) {

        const { page = 1 } = request.query;
        const { limit = 10 } = request.query;

        try {
            var investment = await Investment.paginate({user: request.params.user},{
                page, 
                limit                
            });
        } catch (exception) {
            return response.status(406).send(exception.message);
        }       

        response.json(investment);
    },

     /**
     * Retorna dados de todos os investimentos
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async get(request, response) {

        const { page = 1 } = request.query;
        const { limit = 10 } = request.query;

        try {
            var investment = await Investment.paginate({},{
                page, 
                limit                
            });
        } catch (exception) {
            return response.status(406).send(exception.message);
        }       

        response.json(investment);
    },
    
    
    /**
     * Remove investimento 
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async delete(request, response) {
        try {
            await Investment.findOneAndDelete({_id: request.params.id});
        } catch (exception) {
            return response.status(406).send(exception.message);
        }
        response.send();
    },

    /**
     * Remove investimentos do usuário
     * 
     * @param {*} request 
     * @param {*} response 
     */
    async deleteUser(request, response) {
        try {
            await Investment.deleteMany({user: request.params.user});
        } catch (exception) {
            return response.status(406).send(exception.message);
        }
        response.send();
    }
};