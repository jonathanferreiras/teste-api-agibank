const axios = require('axios')

const urlBase = 'http://localhost:3000/'

module.exports = {

    async RequestPost(req, endpoint) {

        url = urlBase + endpoint
        const result = await axios.post(url,req)
        return result.data
    },

    async RequestGet(req, endpoint) {

        url = urlBase + endpoint 
        const result = await axios.get(url,req)
        return result.data
    },

    async RequestDelete(req, endpoint) {

        url = urlBase + endpoint 
        const result = await axios.delete(url,req)
        return result.status
    },

    mapUser(item) {
        return {
            name: item.name,
            account: item.account,
            agency: item.agency,
            balance: item.balance
        }
    },

    mapInvestment(item) {
        return {
            balance: item.balance,
            type: item.type
        }
    }




}