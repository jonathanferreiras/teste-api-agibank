const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,    
    },
    account: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    agency: {
        type: String,
        required: true,       
        lowercase: true
    },
    balance: {
        type: Number,
    }
}, {
    timestamps: true,
})


UserSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('User', UserSchema)