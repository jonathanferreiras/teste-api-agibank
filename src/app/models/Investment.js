const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate');

const InvestmentSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
       
    },
    balance: {
        type: Number,
    },
    type: {
        type: String,
        required: true,       
        lowercase: true
    }
}, {
    timestamps: true,
})

InvestmentSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Investment', InvestmentSchema)