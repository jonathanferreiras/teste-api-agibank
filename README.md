# Teste API Agibank

Teste de API realizado para o banco Agibank.

#####DESENVOLVIMENTO BÁSICO:
- Crie projeto privado no GitLab, disponibilizando sua documentação (descrição,
configuração e execução), e conceda acesso desenvolvedor aos avaliadores;
- Configure um projeto de Teste em sua linguagem de preferência, com seu respectivo
gerenciador de projeto (Gradle, Maven, Bundler, NPM, VirtualEnv ou PipEnv, etc);
- Realize ao menos um teste positivo para cada método, sendo no mínimo 4;
- Crie, adicionalmente, ao menos dois testes para excessões;

#####DESENVOLVIMENTO IDEAL:
- Criar sua própria API;
- Usar mocks para valida API;




### Instalação

1. Instale Node.js no seu computador.

2. Clone / baixe esta pasta para o seu computador.

3. Executar o seguinte comando dentro desta pasta:
`$ yarn install`

4. Talvez seja necessário instalar a biblioteca Mocha globalmente com o comando:   `yarn global add mocha`

5. Para iniciar a API execute o seguinte comando:
`$ yarn start`

6. a API irá rodar no endereço:
`localhost:3000`

7. Após a API estiver rodando acesse o endereço `http://localhost:3000/api-docs` para acessar a documentação.
![](/readme_img/swagger.jpg)

8. Para realizar os testes rode o comando:
`mocha src/app/test`

9. Os testes seguem na ordem conforme a figura abaixo:

![](/readme_img/apitest.jpg)


### Informações

Esta API foi realizada com o intuito de realizar o teste, sendo bem básica, simulando um banco com contas correntes e contas para investimentos.

### Dependências

 -     "axios": "^0.19.1"
 -     "cors": "^2.8.5"
 -     "express": "^4.17.1"
 -     "global": "^4.4.0"
 -     "mocha": "^7.0.0"
 -     "mongoose": "^5.8.7"
 -     "mongoose-paginate": "^5.0.3"
 -     "swagger-jsdoc": "^3.5.0",
 -     "swagger-ui-express": "^4.1.2"
